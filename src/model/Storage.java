package model;

import java.util.ArrayList;


public interface Storage {

	public boolean loginCheck( String username, String password );

	public ArrayList<String> getPersonalInfo( String username, String password );

	public ArrayList<String> getGrades( String username, String password );
}