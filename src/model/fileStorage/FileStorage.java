package model.fileStorage;

import java.util.ArrayList;
import java.util.List;

import model.Storage;


public class FileStorage implements Storage{

	private static final String PATH = "C://students.txt";


	private TxtParser txtParser = new TxtParser();

	private Tokenizer tokenizer = new Tokenizer();


	public boolean loginCheck( String username, String password ) {

		ArrayList<String> lines = txtParser.read( PATH );

		for( int i = 0; lines != null && i < lines.size(); ++i ) {

			String line = lines.get( i );

			List<String> tokens = tokenizer.tokenize( line, ',' );

			if( tokens.get( 0 ).equals( username ) && tokens.get( 1 ).equals( password ) ) return true;
		}

		return false;
	}

	public ArrayList<String> getPersonalInfo( String username, String password ){

		ArrayList<String> info = new ArrayList<String>();

		ArrayList<String> lines = txtParser.read( PATH );

		for( int i = 0; lines != null && i < lines.size(); ++i ) {

			String line = lines.get( i );

			List<String> tokens = tokenizer.tokenize( line, ',' );

			if( tokens.get( 0 ).equals( username ) && tokens.get( 1 ).equals( password ) ) {

				info.add( tokens.get( 2 ) );
				info.add( tokens.get( 3 ) );
			}
		}

		return info;
	}

	public ArrayList<String> getGrades( String username, String password ){

		ArrayList<String> grades = new ArrayList<String>();

		ArrayList<String> lines = txtParser.read( PATH );

		for( int i = 0; lines != null && i < lines.size(); ++i ) {

			String line = lines.get( i );

			List<String> tokens = tokenizer.tokenize( line, ',' );

			if( tokens.get( 0 ).equals( username ) && tokens.get( 1 ).equals( password ) ) {

				for( int j = 4; tokens != null && j < tokens.size(); ++j ) grades.add( tokens.get( j ) );
			}
		}

		return grades;
	}
}