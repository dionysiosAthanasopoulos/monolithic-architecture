package view.CLI;

import java.util.List;

import controller.StorageController;
import controller.StorageType;


public class StudentCLI {

	public static void main( String[] args ) {

		try{

			StdInput stdInput = new StdInput();

			StorageController storageController = new StorageController();


			String username = null;
			do{ username = stdInput.read( "username" ); }
			while( username != null && username.isEmpty() );

			String password = null;
			do{ password = stdInput.read( "password"); }
			while( password != null && password.isEmpty() );


			boolean result = storageController.loginCheck( username, password, StorageType.file );


			while( result == true ){

				System.out.println( "\n1. View Personal Details" );
				System.out.println( "2. View Grades" );
				System.out.println( "3. Exit" );
				String choice = stdInput.read( "choice" );


				if( choice.equals( "1" ) ) {

					List<String> info = storageController.getPersonalInfo( username, password, StorageType.file );

					System.out.println( info );
				}

				else if( choice.equals( "2" ) ) {

					List<String> info = storageController.getGrades( username, password, StorageType.file );

					System.out.println( info );
				}

				else if( choice.equals( "3" ) ) break;
			}
		}

		catch( Exception ex ) { ex.printStackTrace(); }
	}
}
