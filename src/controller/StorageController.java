package controller;

import java.util.ArrayList;

import model.Storage;
import model.fileStorage.FileStorage;


public class StorageController {

	public boolean loginCheck( String username, String password, StorageType storageType ) {

		if( storageType == StorageType.file ) {

			Storage fileStorage = new FileStorage();

			return fileStorage.loginCheck( username, password );
		}

		return false;
	}

	public ArrayList<String> getPersonalInfo( String username, String password, StorageType storageType ){

		if( storageType == StorageType.file ) {

			Storage fileStorage = new FileStorage();

			return fileStorage.getPersonalInfo( username, password );
		}

		return null;
	}

	public ArrayList<String> getGrades( String username, String password, StorageType storageType ){

		if( storageType == StorageType.file ) {

			Storage fileStorage = new FileStorage();

			return fileStorage.getGrades( username, password );
		}

		return null;
	}
}
